package pa.soft.view;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import pa.soft.model.Entry;
import pa.soft.model.Severity;

public class EntryView
{
	private static final String FX_ALIGNMENT_CENTER = "-fx-alignment: CENTER;";
	private final TableView<Entry> viewNode = new TableView<>();

	public EntryView()
	{
		TableColumn<Entry, Node> severityColumn = new TableColumn<>("Schwere");
		severityColumn.setCellValueFactory(param -> new SimpleObjectProperty<>(Severity.findSeverity(param.getValue()).getView()));
		severityColumn.setStyle(FX_ALIGNMENT_CENTER);

		TableColumn<Entry, String> dateTimeColumn = new TableColumn<>("Zeitpunkt");
		dateTimeColumn.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getLocalDateTimeString()));

		TableColumn<Entry, Integer> systolischColumn = new TableColumn<>("Sys.");
		systolischColumn.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getSystolisch()));
		systolischColumn.setStyle(FX_ALIGNMENT_CENTER);

		TableColumn<Entry, Integer> diastolischColumn = new TableColumn<>("Dia.");
		diastolischColumn.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getDiastolisch()));
		diastolischColumn.setStyle(FX_ALIGNMENT_CENTER);

		TableColumn<Entry, Integer> pulsColumn = new TableColumn<>("Puls");
		pulsColumn.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getPuls()));
		pulsColumn.setStyle(FX_ALIGNMENT_CENTER);

		TableColumn<Entry, String> bemerkungColumn = new TableColumn<>("Bemerkung");
		bemerkungColumn.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getBemerkung()));

		viewNode.getColumns().addAll(severityColumn, dateTimeColumn, systolischColumn, diastolischColumn, pulsColumn, bemerkungColumn);
	}

	public Node getViewNode()
	{
		return viewNode;
	}

	public void setItems(final ObservableList<Entry> entries)
	{
		viewNode.setItems(entries);
	}
}
