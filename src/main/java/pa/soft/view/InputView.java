package pa.soft.view;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import pa.soft.control.actions.SpeicherAction;
import pa.soft.control.actions.EintragenAction;
import pa.soft.model.Entry;
import pa.soft.util.LocalDateTimeProvider;

import java.io.File;
import java.util.function.UnaryOperator;

public class InputView
{
	private static final int DEFAULT_PADDING = 10;
	private static final UnaryOperator<TextFormatter.Change> INTEGER_FILTER = change -> {
		String newText = change.getControlNewText();
		if (newText.matches("-?([1-9][0-9]*)?")) {
			return change;
		}
		return null;
	};

	private EintragenAction eintragenAction;
	private SpeicherAction csvSpeichernAction;
	private SpeicherAction htmlSpeichernAction;
	private final Button eintragen;
	private final MenuButton export;
	private final TextField date;
	private final TextField systolisch;
	private final TextField diastolisch;
	private final TextField puls;
	private final TextField bemerkung;

	private final HBox viewNode = new HBox(DEFAULT_PADDING);

	public InputView()
	{
		date = new TextField(LocalDateTimeProvider.now());
		viewNode.getChildren().add(makeInput(date, "Datum"));

		systolisch = new TextField();
		viewNode.getChildren().add(makeNarrowInput(systolisch, "Sys."));

		diastolisch = new TextField();
		viewNode.getChildren().add(makeNarrowInput(diastolisch, "Dia."));

		puls = new TextField();
		viewNode.getChildren().add(makeNarrowInput(puls, "Puls"));

		bemerkung = new TextField();
		viewNode.getChildren().add(makeInput(bemerkung, "Bemerkung"));

		eintragen = new Button("Eintragen");
		export = new MenuButton("Export");
		makeButtons();

		styleControls();
	}

	private void makeButtons()
	{
		VBox buttons = new VBox(DEFAULT_PADDING);
		eintragen.setMaxWidth(Double.MAX_VALUE);
		eintragen.setOnAction(event -> eintragenAction.eintragen(createEntry()));

		buttons.getChildren().addAll(eintragen, makeMenuButton());
		viewNode.getChildren().add(buttons);
	}

	private MenuButton makeMenuButton()
	{
		MenuItem csvExport = new MenuItem("CSV");
		csvExport.setOnAction(event -> csvSpeichernAction.speichern(pickDestinationFile()));
		MenuItem htmlExport = new MenuItem("HTML");
		htmlExport.setOnAction(event -> htmlSpeichernAction.speichern(pickDestinationFile()));
		export.setMaxWidth(Double.MAX_VALUE);
		export.getItems().addAll(csvExport, htmlExport);

		return export;
	}

	private File pickDestinationFile()
	{
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Zieldatei wählen");
		return fileChooser.showSaveDialog(viewNode.getScene().getWindow());
	}

	private VBox makeInput(final TextField textField, final String caption )
	{
		VBox input = new VBox(DEFAULT_PADDING);
		input.getChildren().addAll(new Label(caption), textField);
		return input;
	}

	private VBox makeNarrowInput(final TextField textField, final String caption)
	{
		VBox input = new VBox(DEFAULT_PADDING);
		textField.setTextFormatter(new TextFormatter<>(INTEGER_FILTER));
		input.getChildren().addAll(new Label(caption), textField);
		input.setMaxWidth(50);
		return input;
	}

	private Entry createEntry()
	{
		Entry entry =
				new Entry(
						date.getText(),
						Integer.parseInt(systolisch.getText()),
						Integer.parseInt(diastolisch.getText()),
						Integer.parseInt(puls.getText()),
						bemerkung.getText()
				);
		resetInputFields();

		return entry;
	}

	private void resetInputFields()
	{
		date.setText(LocalDateTimeProvider.now());
		systolisch.setText("");
		diastolisch.setText("");
		puls.setText("");
		bemerkung.setText("");
	}

	private void styleControls()
	{
		String styleNumberInput = "-fx-width: 25px;";

		systolisch.setStyle(styleNumberInput);
		diastolisch.setStyle(styleNumberInput);
		puls.setStyle(styleNumberInput);

		viewNode.setStyle("-fx-padding: 5px");
	}

	public Node getViewNode()
	{
		return viewNode;
	}

	public void setEintragenAction(final EintragenAction eintragenAction)
	{
		this.eintragenAction = eintragenAction;
	}

	public void setCsvSpeichernAction(final SpeicherAction speicherAction)
	{
		this.csvSpeichernAction = speicherAction;
	}

	public void setHtmlSpeichernAction(final SpeicherAction speicherAction)
	{
		this.htmlSpeichernAction = speicherAction;
	}
}
