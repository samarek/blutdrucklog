package pa.soft.view;

import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import pa.soft.control.EntryControl;
import pa.soft.control.InputControl;
import pa.soft.control.export.CsvExporter;
import pa.soft.control.export.HtmlExporter;

public class MainView
{
	private final BorderPane viewNode = new BorderPane();

	public MainView()
	{
		final InputControl inputControl = new InputControl();
		final EntryControl entryControl = new EntryControl();
		final CsvExporter csvExporter = new CsvExporter();
		final HtmlExporter htmlExporter = new HtmlExporter();

		inputControl.setEintragenAction(entryControl::eintragen);
		inputControl.setCsvSpeichernAction(destination -> csvExporter.exportEntries(entryControl.getEntries(), destination));
		inputControl.setHtmlSpeichernAction(destination -> htmlExporter.exportEntries(entryControl.getEntries(), destination));

		viewNode.setTop(inputControl.getView());
		viewNode.setCenter(entryControl.getView());
	}

	public Node getViewNode()
	{
		return viewNode;
	}
}
