package pa.soft.control;

import javafx.scene.Node;
import pa.soft.control.actions.SpeicherAction;
import pa.soft.control.actions.EintragenAction;
import pa.soft.view.InputView;

public class InputControl
{
	private InputView inputView = new InputView();

	public void setEintragenAction(EintragenAction eintragenAction)
	{
		inputView.setEintragenAction(eintragenAction);
	}

	public void setCsvSpeichernAction(SpeicherAction csvSpeichernAction)
	{
		inputView.setCsvSpeichernAction(csvSpeichernAction);
	}

	public void setHtmlSpeichernAction(SpeicherAction csvSpeichernAction)
	{
		inputView.setHtmlSpeichernAction(csvSpeichernAction);
	}

	public Node getView()
	{
		if(inputView == null)
		{
			inputView = new InputView();
		}

		return inputView.getViewNode();
	}
}
