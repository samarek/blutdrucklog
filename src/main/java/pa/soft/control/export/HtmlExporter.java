package pa.soft.control.export;

import javafx.collections.ObservableList;
import pa.soft.model.Entry;
import pa.soft.model.Severity;

import java.io.File;

public class HtmlExporter extends FileAccessor
{
	private static final String TD_STYLE = "style=\"padding:5px; border: 1px solid black;\"";
	private static final String STYLED_TD = "<td " + TD_STYLE + ">";
	private static final String CLOSED_TD = "</td>";
	private static final String HTML_TABLE_HEAD_LINE = STYLED_TD + "Schwere" + CLOSED_TD
			+ STYLED_TD + "Datum" + CLOSED_TD
			+ STYLED_TD + "Sys." + CLOSED_TD
			+ STYLED_TD + "Dia." + CLOSED_TD
			+ STYLED_TD + "Puls" + CLOSED_TD
			+ STYLED_TD + "Bemerkung" + CLOSED_TD + System.lineSeparator();


	public void exportEntries(final ObservableList<Entry> entries, final File destination)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<html>").append(System.lineSeparator());
		sb.append("<head>").append("</head>").append(System.lineSeparator());
		sb.append("<body>").append(System.lineSeparator());
		sb.append("<table style=\"border-collapse: collapse;\">").append(System.lineSeparator());
		sb.append(HTML_TABLE_HEAD_LINE).append(System.lineSeparator());

		entries.forEach(entry -> {
			sb.append("<tr>").append(System.lineSeparator());
			sb.append("<td " + TD_STYLE + " align=\"center\">").append(Severity.findSeverity(entry).getHtml()).append(CLOSED_TD);
			sb.append(toHtmlTableData(entry));
			sb.append("</tr>").append(System.lineSeparator());
		});

		sb.append("</table>").append(System.lineSeparator());
		sb.append("</body>").append(System.lineSeparator());
		sb.append("</html>").append(System.lineSeparator());

		writeFile(sb.toString(), destination);
	}

	private String toHtmlTableData(final Entry entry)
	{
		return  STYLED_TD + entry.getLocalDateTimeString() + CLOSED_TD +
				STYLED_TD + entry.getSystolisch() + CLOSED_TD +
				STYLED_TD + entry.getDiastolisch() + CLOSED_TD +
				STYLED_TD + entry.getPuls() + CLOSED_TD +
				STYLED_TD + entry.getBemerkung() + CLOSED_TD;
	}
}
