package pa.soft.control.export;

import pa.soft.model.Entry;

import java.io.File;
import java.util.List;

public class CsvExporter extends FileAccessor
{
	private static final String CSV_HEAD_LINE = "Datum;Sys.;Dia.;Puls;Bemerkung";

	public void exportEntries(List<Entry> entries, File destination)
	{
		appendLine(CSV_HEAD_LINE, destination);
		entries.forEach(entry -> writeEntry(entry, destination));
	}

	public static String getCsvHeadLine()
	{
		return CSV_HEAD_LINE;
	}
}
