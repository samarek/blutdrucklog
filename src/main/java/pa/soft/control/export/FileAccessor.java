package pa.soft.control.export;

import pa.soft.model.Entry;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

public abstract class FileAccessor
{
	private static final File APPLICATION_DIRECTORY = new File(System.getProperty("user.home") + "/.blutdrucklog");
	private static final File ERROR_FILE = new File(APPLICATION_DIRECTORY + "/error.log");

	protected void writeEntry(Entry entry, File destination)
	{
		appendLine(entry.toCsv(), destination);
	}

	void writeFile(String line, final File destination)
	{
		writeLine(line, destination, false);
	}

	protected void appendLine(String line, final File destination)
	{
		writeLine(line, destination, true);
	}

	private void writeLine(String line, final File destination, final boolean append)
	{
		try (Writer writer = new FileWriter(destination, append))
		{
			writer.append(line).append(System.lineSeparator());
			writer.flush();
		}
		catch(IOException e)
		{
			logException(e);
		}
	}

	protected void logException(Exception e)
	{
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		String sStackTrace = sw.toString();

		logError("### Exception ###" + System.lineSeparator() + sStackTrace);
	}

	protected void logError(String error)
	{
		appendLine(error, ERROR_FILE);
	}

	protected File getApplicationDirectory()
	{
		return APPLICATION_DIRECTORY;
	}
}
