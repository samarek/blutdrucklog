package pa.soft.control;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import pa.soft.model.Entry;
import pa.soft.util.LogAccessor;
import pa.soft.view.EntryView;

public class EntryControl
{
	private final EntryView entryView = new EntryView();
	private final LogAccessor logAccessor = new LogAccessor();
	private final ObservableList<Entry> entries = FXCollections.observableArrayList(logAccessor.loadEntries());

	public EntryControl()
	{
		entryView.setItems(entries);
	}

	public Node getView()
	{

		return entryView.getViewNode();
	}

	public void eintragen(final Entry entry)
	{
		entries.add(entry);
		logAccessor.writeLogEntry(entry);
	}

	public ObservableList<Entry> getEntries()
	{
		return entries;
	}
}
