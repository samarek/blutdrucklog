package pa.soft.control;

import javafx.scene.Node;
import javafx.scene.Parent;
import pa.soft.view.MainView;

public class MainControl
{
	private MainView mainView;

	public static Parent start()
	{
		MainControl mainControl = new MainControl();
		return (Parent) mainControl.getView();
	}

	private Node getView()
	{
		if(mainView == null)
		{
			mainView = new MainView();
		}

		return mainView.getViewNode();
	}
}
