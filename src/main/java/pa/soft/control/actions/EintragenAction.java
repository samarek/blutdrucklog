package pa.soft.control.actions;

import pa.soft.model.Entry;

public interface EintragenAction
{
	void eintragen(Entry entry);
}
