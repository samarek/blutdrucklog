package pa.soft.control.actions;

import java.io.File;

public interface SpeicherAction
{
	void speichern(File destination);
}
