package pa.soft.util;

import pa.soft.control.export.CsvExporter;
import pa.soft.control.export.FileAccessor;
import pa.soft.model.Entry;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

public class LogAccessor extends FileAccessor
{
	private final File logFile = new File(getApplicationDirectory() + "/blutdruck.log");

	public List<Entry> loadEntries()
	{
		ensureApplicationDirectory();

		final List<Entry> logEntries = new ArrayList<>();
		if(logFile.exists())
		{
			logEntries.addAll(readEntries());
		}
		else
		{
			createFile(logFile);
		}

		return logEntries;
	}

	private void ensureApplicationDirectory()
	{
		ensureDirectory(getApplicationDirectory());
	}

	private Collection<? extends Entry> readEntries()
	{
		final List<Entry> logEntries = new ArrayList<>();
		try (Scanner scanner = new Scanner(logFile))
		{
			while(scanner.hasNext())
			{
				String line = scanner.nextLine();
				if(!line.isEmpty() && !line.startsWith("Datum"))
				{
					logEntries.add(Entry.fromCSV(line));
				}
			}
		}
		catch(FileNotFoundException e)
		{
			logException(e);
		}

		return logEntries;
	}

	private void createFile(final File file)
	{
		try
		{
			if(file.createNewFile())
			{
				appendLine(CsvExporter.getCsvHeadLine(), file);
			}
		}
		catch(IOException e)
		{
			logException(e);
		}
	}

	private void ensureDirectory(File directory)
	{
		if(!directory.exists() && !directory.mkdirs())
		{
			logError("Couldn't create " + directory.getAbsolutePath());
		}
	}

	public void writeLogEntry(Entry entry)
	{
		writeEntry(entry, logFile);
	}
}
