package pa.soft.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateTimeProvider
{
	private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

	private LocalDateTimeProvider()
	{

	}

	public static String format(LocalDateTime localDateTime)
	{
		return DATE_TIME_FORMATTER.format(localDateTime);
	}

	public static LocalDateTime parse(String localDateTime)
	{
		return LocalDateTime.parse(localDateTime, DATE_TIME_FORMATTER);
	}

	public static String now()
	{
		return DATE_TIME_FORMATTER.format(LocalDateTime.now());
	}
}
