package pa.soft;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import pa.soft.control.MainControl;

public class Main extends Application
{
	@Override
	public void start(Stage primaryStage)
	{
		Parent root = MainControl.start();
		primaryStage.setTitle("Blutdruck Log");
		primaryStage.setScene(new Scene(root, 800, 600));
		String iconPath = getClass().getResource("/icon_64.png").toExternalForm();
		primaryStage.getIcons().add(new Image(iconPath));
		primaryStage.show();
	}

	public static void main(String[] args)
	{
		launch(args);
	}
}
