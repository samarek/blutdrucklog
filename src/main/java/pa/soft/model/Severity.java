package pa.soft.model;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public enum Severity
{
	OPTIMAL(Color.LIGHTGREEN),
	NORMAL(Color.GREEN),
	HIGH_NORMAL(Color.DARKGREEN),
	LIGHT_HYPERTONY(Color.YELLOW),
	MEDIUM_HYPERTONY(Color.ORANGE),
	SEVERE_HYPERTONY(Color.RED);

	private final Color color;

	Severity(final Color color)
	{
		this.color = color;
	}

	public Node getView()
	{
		Rectangle viewNode = new Rectangle(20,10, color);
		viewNode.setStroke(Color.BLACK);

		return viewNode;
	}

	public String getHtml()
	{
		return  "<svg width=\"20\" height=\"10\">" + System.lineSeparator()
				+ "<rect width=\"20\" height=\"10\" style=\"fill:" + toRGBCode() + ";stroke-width:1;stroke:#000\" />" + System.lineSeparator()
				+ "</svg>" + System.lineSeparator();
	}

	private String toRGBCode()
	{
		return String.format( "#%02X%02X%02X",
							  (int)( color.getRed() * 255 ),
							  (int)( color.getGreen() * 255 ),
							  (int)( color.getBlue() * 255 ) );
	}

	public static Severity findSeverity(Entry entry)
	{
		final int syst = entry.getSystolisch();
		final int diast = entry.getDiastolisch();

		if(syst >= 140 || diast >= 90)
		{
			return findHypertony(syst, diast);
		}
		else
		{
			return findNormal(syst, diast);
		}
	}

	private static Severity findHypertony(int syst, int diast)
	{
		if(syst >= 180 || diast >= 110)
		{
			return SEVERE_HYPERTONY;
		}
		else if(syst >= 160 || diast >= 100)
		{
			return MEDIUM_HYPERTONY;
		}
		else
		{
			return LIGHT_HYPERTONY;
		}
	}

	private static Severity findNormal(int syst, int diast)
	{
		if(syst >= 130 || diast >= 85)
		{
			return HIGH_NORMAL;
		}
		else if(syst >= 120 || diast >= 80)
		{
			return NORMAL;
		}
		else
		{
			return OPTIMAL;
		}
	}

}
