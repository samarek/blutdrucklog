package pa.soft.model;

import pa.soft.util.LocalDateTimeProvider;

import java.time.LocalDateTime;

public class Entry
{
	private final LocalDateTime localDateTime;
	private final int systolisch;
	private final int diastolisch;
	private final int puls;
	private final String bemerkung;

	public Entry(final String localDateTime, final int systolisch, final int diastolisch, final int puls, final String bemerkung)
	{
		this.localDateTime = LocalDateTimeProvider.parse(localDateTime);
		this.systolisch = systolisch;
		this.diastolisch = diastolisch;
		this.puls = puls;
		this.bemerkung = bemerkung;
	}

	public static Entry fromCSV(String csv)
	{
		final String[] csvEntries = csv.split(";");

		return new Entry(
				csvEntries[0],
				Integer.parseInt(csvEntries[1]),
				Integer.parseInt(csvEntries[2]),
				Integer.parseInt(csvEntries[3]),
				csvEntries.length > 4 ? csvEntries[4] : ""
		);
	}

	public String getLocalDateTimeString()
	{
		return LocalDateTimeProvider.format(localDateTime);
	}

	public int getSystolisch()
	{
		return systolisch;
	}

	public int getDiastolisch()
	{
		return diastolisch;
	}

	public int getPuls()
	{
		return puls;
	}

	public String getBemerkung()
	{
		return bemerkung;
	}

	@Override
	public String toString()
	{
		return "Entry{"
				+ "localDateTime=" + LocalDateTimeProvider.format(localDateTime)
				+ ", systolisch=" + systolisch
				+ ", diastolisch=" + diastolisch
				+ ", puls=" + puls
				+ ", bemerkung='" + bemerkung
				+ "'}";
	}

	public String toCsv()
	{
		return LocalDateTimeProvider.format(localDateTime) + ";" + systolisch + ";" + diastolisch + ";" + puls + ";" + bemerkung;
	}
}
